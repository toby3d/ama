# Ask Me Anything <sup>[English](README.md)</sup>
👋🙂 Привет, я [toby3d](https://toby3d.ru/)!

* Задать вопрос через:
  * [GitLab](https://gitlab.com/toby3d/ama/-/issues/new)
  * [Email](mailto:incoming+toby3d-ama-17601127-issue-@incoming.gitlab.com)
* [Просмотреть ответы](https://gitlab.com/toby3d/ama/-/issues?state=closed)

## Темы
Ты можешь спросить меня **буквально о чём угодно**. Если будет что-то, про что
мне абсолютно ничего неизвестно, то я постыдно закрою вопрос с меткой [idk](https://gitlab.com/toby3d/ama/-/issues?label_name%5B%5D=idk).
~~*Гоните его, насмехайтесь над ним!*~~

Есть несколько тем в которые я погружён, заинтересован или которые каким-либо
образом меня затрагивают:

* [Go](https://golang.org)
* [Godot](https://godotengine.org)
* [Hugo](https://gohugo.io)
* [Svelte](https://svelte.dev)
* [Telegram](https://telegram.org)
* CSS
* HTML
* Аниме и хентай
* Видеоигры и геймдев
* Доступность в вэбе
* Мемы
* Прослушивание и производство музыки
* Фортепиано и синтезаторы
