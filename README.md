# Ask Me Anything <sup>[Русский](README.ru.md)</sup>
👋🙂 Hi, I'm [toby3d](https://toby3d.me)!

* Ask a question via:
  * [GitLab](https://gitlab.com/toby3d/ama/-/issues/new)
  * [Email](mailto:incoming+toby3d-ama-17601127-issue-@incoming.gitlab.com)
* [Read answers](https://gitlab.com/toby3d/ama/-/issues?state=closed)

## Topics
You can ask me **literally anything** you want. If there's something I don't
know nothing about, I'll shamefully close with the [idk](https://gitlab.com/toby3d/ama/-/issues?label_name%5B%5D=idk) label.
~~*Drive him away, make a laugh at him!*~~

There are several topics that I'm immersed, interested in or somehow affecting me:

* [Go](https://golang.org)
* [Godot](https://godotengine.org)
* [Hugo](https://gohugo.io)
* [Svelte](https://svelte.dev)
* [Telegram](https://telegram.org)
* Accessibility in the web
* Anime & hentai
* CSS
* HTML
* Listening & making music
* Memes
* Piano & synthesizers
* Video games & gamedev
